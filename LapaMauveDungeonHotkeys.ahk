;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; AutoHotKey script for Lapa's Dungeon Game
; Download AHK: https://autohotkey.com/download/
;
; Enter Game Discord: https://discord.gg/eR84Gff
; Enter Discord Channel: Games > #dungeon
;
; Author: ShubiMaja
; Date: 02/07/2018
; License: MIT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Utility Actions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!g:: ; Alt + G
Send, {!}go{Enter} ; Go!
Send, {!}c{Enter} ; Remind us what color we are
return

!f:: ; Alt + F
Send, Move off spawn, please{!}{Enter} ; For when Go! fails
return

!c:: ; Alt + C
Send, {!}c{Enter} ; Get Your Color
return

^k:: ; Ctrl + K
Send, {!}b WoodenKey{Enter} ; Buy Wooden Key
return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Move Actions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!up:: ; Alt + Up
Send, {!}mu{Enter} ; Move Up 
return

!down:: ; Alt + Down
Send, {!}md{Enter} ; Move Down
return

!left:: ; Alt + Left
Send, {!}ml{Enter} ; Move Left
return

!right:: ; Alt + Right
Send, {!}mr{Enter} ; Move Right
return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Attack Actions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

^up:: ; Ctrl + Up
Send, {!}au{Enter} ; Attack Up 
return

^down:: ; Ctrl + Down
Send, {!}ad{Enter} ; Attack Down
return

^left:: ; Ctrl + Left
Send, {!}al{Enter} ; Attack Left
return

^right:: ; Ctrl + Right
Send, {!}ar{Enter} ; Attack Right
return